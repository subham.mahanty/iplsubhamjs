const csvtojsonV2=require("csvtojson");

// deliveries.csv
csvFilePath='/Users/subhamkumarmahanty/Documents/iplJavaScript/ipl/deliveries.csv'
const csv=require('csvtojson')
csv()
.fromFile(csvFilePath)
.then((jsonObjDeliveries)=>{

// matches.csv
csvFilePath2='/Users/subhamkumarmahanty/Documents/iplJavaScript/ipl/matches.csv'
csv()
.fromFile(csvFilePath2)
.then((jsonObjMatches)=>{

    // Q. 1: Plot the number of matches played per year of all the years in IPL.
    console.log("\nQ. 1: Plot the number of matches played per year of all the years in IPL.\n");
    const matchesPlayedPerYear = () => {
        var newObj = {};
        newObj = jsonObjMatches.reduce((total, value) => {
            if(value.season in total){
                total[value.season]++;
            }
            else{
                total[value.season] = 1;
            }
            return total;
        },{});
        return newObj;
    }
    console.log(matchesPlayedPerYear());

    // Q. 2: Plot matches won of all teams over all the years of IPL.
    console.log("\nQ. 2: Plot matches won of all teams over all the years of IPL.\n");
    const matchesWonAllTeamsOverAllYears = () => {
        var newObj = {};
        newObj =  jsonObjMatches.reduce((total, value) => {
            if(value.season in total){
                if(total[value. season].hasOwnProperty(value.winner)){
                    total[value.season][value.winner]++;
                }
                else{
                    total[value.season][value.winner] = 1;
                }
            }
            else{
                total[value.season] = {};
                total[value.season][value.winner] = 1;
            }
            return total;
        },{});
        return newObj;
    }
    console.log(matchesWonAllTeamsOverAllYears());

    // Q. 3: For the year 2016 plot the extra runs conceded per team.
    console.log("\nQ. 3: For the year 2016 plot the extra runs conceded per team.\n");
    const getExtraRunsPerTeamForYear = () => {
        const opObj = { '2016': {} };
        const valueObj = {};
        jsonObjMatches.filter(match => match['season'] === '2016').map(singleMatch => {
            jsonObjDeliveries.map(delivery => {
                if (singleMatch['id'] === delivery['match_id']) {
                    if (valueObj.hasOwnProperty(delivery['bowling_team'])) {
                        valueObj[delivery['bowling_team']] += parseInt(delivery['extra_runs'])
                    } else {
                        valueObj[delivery['bowling_team']] = parseInt(delivery['extra_runs'])
                    }
                }
            });
        });
        opObj['2016'] = valueObj;
        return opObj;
    }
    console.log(getExtraRunsPerTeamForYear());

// Q.4 )    For the year 2015 plot the top economical bowlers.
    console.log("\nQ. 4: For the year 2015 plot the top economical bowlers.\n");
    const topEconomicalBowler = () => {
        const temp = {'2015' : {} };
        const latestObj = {};
        jsonObjMatches.filter(match => match['season'] === '2015').map(singleDelivery => {
            jsonObjDeliveries.map(delivery => {
                if(singleDelivery['id'] === delivery['match_id']) {
                    if (latestObj.hasOwnProperty(delivery['bowler'])) {
                        latestObj[delivery['bowler']]['runs'] +=
                          parseInt(delivery['total_runs']) -
                          parseInt(delivery['bye_runs']) -
                          parseInt(delivery['legbye_runs']) -
                          parseInt(delivery['penalty_runs'])
            
                        if (delivery['ball'] == 6) {
                          latestObj[delivery['bowler']]['overs']++;
                        }
                    } else {
                        latestObj[delivery['bowler']] = {
                          runs :
                            parseInt(delivery['total_runs']) -
                            parseInt(delivery['bye_runs']) -
                            parseInt(delivery['legbye_runs']) -
                            parseInt(delivery['penalty_runs']),

                          overs: 0
                        };
                    }
                }
            })
        })

        var arrBowlerAndEconomyRates = [];
        var result = Object.entries(latestObj).filter(function(value){
            //  console.log(value[0]);
            arrBowlerAndEconomyRates.push([ value[0], (latestObj[value[0]]['runs'] / latestObj[value[0]]['overs']).toFixed(2) ])
        });
        arrBowlerAndEconomyRates.sort( function (a, b) {
            return a[1] - b[1];
        })
        var arrBowlerAndEconomyRatesModified = arrBowlerAndEconomyRates.slice(0, 5)

        var finalObj = {};
        arrBowlerAndEconomyRatesModified.map(element => {
            finalObj[element[0]] = element[1];
        })

        return finalObj;
    }

    console.log(topEconomicalBowler());

})  // scope of matches file

})  // scope of deliveries file

